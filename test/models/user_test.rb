require 'test_helper'

class Api::V1::UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new({
      name: 'Test User',
      email: 'test@gmail.com',
      password: 'securecode'
    })
  end

  test 'valid user' do
    assert @user.valid?
  end

  test 'invalid without name' do
    @user.name = nil
    refute @user.valid?, 'saved user without a name'
    assert_not_nil @user.errors[:name], 'no validation error for name present'
  end

  test 'invalid without email' do
    @user.email = nil
    refute @user.valid?
    assert_not_nil @user.errors[:email]
  end

  test "invalid without password" do
    @user.password = nil
    refute @user.valid?
    assert_not_nil @user.errors[:password]
  end

  test "only save if email is unique" do
    @user_copy = @user.dup
    @user.save
    @user_copy.save
    assert_includes @user_copy.errors, :email
    assert @user_copy.errors.full_messages.include?("Email has already been taken")
  end
end
